require_relative '../Awesome.rb'

RSpec.describe Awesome do

	before(:all) do

		people = (1..100).collect do |n|
			{name: "Person #{n}", awesomeness: n * n}
		end

		@awesome = Awesome.new(people.shuffle)
	end

	it "contains the correct length for people given" do
		expect(@awesome.people.length).to eq 100
	end

	it "returns the most awesome person" do
		expect(@awesome.most[:name]).to eq('Person 100')
		expect(@awesome.most[:awesomeness]).to eq 10000
	end

	it "correctly computes the average awesomeness for the given set" do
		expect(@awesome.average).to eq 3383
	end

	describe "top ten awesome people" do

		let(:top) { 100.downto(91).collect { |i| "Person #{i}" } }

		it "displays a sorted list of the top n most awesome people" do
			@awesome.top(10).each_with_index do |p, i|
				expect(p[:name]).to eq top.at(i)
			end
		end

		it "prints out a list of the top ten people" do
			expect { @awesome.top_ten }.to output(top.map{|p| p << "\n"}.join).to_stdout 
		end
	end

end
