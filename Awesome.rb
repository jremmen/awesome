class Awesome
	
	attr_reader :people
	
	def initialize(people)
		@people = people.sort { |a, p| p[:awesomeness] <=> a[:awesomeness] }
	end

	def most
		@people.first
	end

	def average
		@average ||= @people.inject(0) { |t, p| t + p[:awesomeness] } / @people.length
	end

	def top(n)
		@people.take(n)
	end

	def top_ten
		top(10).each { |p| puts p[:name] }
	end

end
